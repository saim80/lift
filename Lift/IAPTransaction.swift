//
//  IAPTransaction.swift
//  Lift
//
//  Created by Sangwoo Im on 8/27/15.
//  Copyright © 2015 Sangwoo Im. All rights reserved.
//

import Foundation
import StoreKit

public enum IAPState : CustomStringConvertible {
    case ready
    case collectingPayment
    case deliveringProduct
    case delivered
    
    public var description : String {
        var outString = ""
        switch (self) {
        case .ready:
            outString = NSLocalizedString("Ready to purchase.", comment: #file)
        case .collectingPayment:
            outString = NSLocalizedString("Processing your payment...", comment: #file)
        case .deliveringProduct:
            outString = NSLocalizedString("Processing...", comment: #file)
        case .delivered:
            outString = NSLocalizedString("Purchase finished successfully!", comment: #file)
        }
        
        return outString
    }
}

public enum IAPError : Error, CustomStringConvertible {
    case paymentCancelled
    case paymentFailed
    case receiptValidationFailed
    case productDeliveryFailed
    
    public var description : String {
        var outString = ""
        
        switch (self) {
        case .paymentCancelled:
            outString = NSLocalizedString("Payment is cancelled.", comment: #file)
        case .paymentFailed:
            outString = NSLocalizedString("Error occurred while processing your payment. Please try again.", comment: #file)
        case .receiptValidationFailed:
            outString = NSLocalizedString("Payment receipt could not be validated.", comment: #file)
        case .productDeliveryFailed:
            outString = NSLocalizedString("Error occurred while delivering the product. Please try again.", comment: #file)
        }
        
        return outString
    }
}

open class IAPTransaction : NSObject {
    open let product : SKProduct
    
    open weak var queue : IAPQueue?
    open var state = IAPState.ready {
        didSet {
            if state != oldValue {
                guard let delegate = queue?.delegate as? IAPQueueDelegate else {
                    return
                }
                
                delegate.IAPQueueDidUpdateTransaction(self)
            }
        }
    }
    
    public init (product:SKProduct, queue:IAPQueue) {
        self.product = product
        self.queue = queue
        super.init()
    }
    
    open var paymentTransaction : SKPaymentTransaction? {
        let queue = SKPaymentQueue.default()
        
        let pendingTransactions = queue.transactions
        
        let filtered = pendingTransactions.filter { (transaction:SKPaymentTransaction) -> Bool in
            return transaction.payment.productIdentifier == self.product.productIdentifier
        }
        
        return filtered.first
    }
    
    open var inPaymentQueue : Bool {
        return paymentTransaction != nil
    }
    
    open func start() throws {
        if let transaction = paymentTransaction {
            let transactionState = transaction.transactionState
            if transactionState == SKPaymentTransactionState.purchased ||
               transactionState == SKPaymentTransactionState.restored
            {
                _ = deliver()
            } else if transactionState != SKPaymentTransactionState.deferred {
                throw IAPError.paymentFailed
            }
        } else {
            try startPaymentTransaction()
        }
    }
    
    fileprivate func startPaymentTransaction() throws {
        self.state = .collectingPayment
        
        let payment = SKPayment(product:self.product)
        
        let queue = SKPaymentQueue.default()
        
        queue.add(payment)
    }
    
    open func deliver() -> Bool {
        self.state = .deliveringProduct
        
        return true
    }
    
    internal func fulfill() throws {
        if self.deliver() {
            self.state = .delivered
        } else {
            throw IAPError.receiptValidationFailed
        }
    }
    
    open func finish() throws {
        if let transaction = paymentTransaction {
            let queue = SKPaymentQueue.default()
            
            if let error = transaction.error as? NSError {
                if error.code == SKError.paymentCancelled.rawValue {
                    throw IAPError.paymentCancelled
                } else {
                    throw IAPError.paymentFailed
                }
            }
            
            queue.finishTransaction(transaction)
        } else if self.state != .delivered {
            throw IAPError.productDeliveryFailed
        }
    }
}

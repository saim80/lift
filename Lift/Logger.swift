//
//  Logger.swift
//  Knot
//
//  Created by Sangwoo Im on 8/3/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

import Foundation

open class Logger {
    private static var __once: () = {
            Static.shared = Logger()
        }()
    public enum Level:Int, CustomStringConvertible {
        case info, debug, warning, critical
        
        public var description:String {
            var outString:String?
            switch self {
            case .info:
                outString = "Info"
            case .debug:
                outString = "Debug"
            case .warning:
                outString = "Warning"
            case .critical:
                outString = "Critical"
            }
            return outString!
        }
    }
    
    fileprivate struct Static {
        static var level:Level              = .info
        static var onceFlag:Int = 0
        static var shared:Logger?           = nil
    }
    
    fileprivate func log(_ message:String, level:Level = .info, file:String, function:String) {
        if Static.level.rawValue <= level.rawValue {
            var fileName = file as NSString
            
            fileName = fileName.lastPathComponent as NSString
            
            print("[\(level.description)]: \(fileName): \(function): \(message)")
        }
    }
    
    open class func shared() -> Logger {
        _ = Logger.__once
    
        return Static.shared!
    }
    
    open class var logLevel:Level {
        set (value) {
            Static.level = value
        }
        get {
            return Static.level
        }
    }
    
    open class func log(_ message:String, file:String = #file, function:String = #function) {
        shared().log(message, file:file, function:function)
    }
    
    open class func logDebug(_ message:String, file:String = #file, function:String = #function) {
        shared().log(message, level: .debug, file:file, function:function)
    }
    
    open class func logWarning(_ message:String, file:String = #file, function:String = #function) {
        shared().log(message, level: .warning, file:file, function:function)
    }
    
    open class func logCritical(_ message:String, file:String = #file, function:String = #function) {
        shared().log(message, level: .critical, file:file, function:function)
    }
}

//
//  Config.swift
//  Mind Trails
//
//  Created by Sangwoo Im on 7/26/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

import Foundation

open class Config {
    fileprivate let rawData    :[String:AnyObject]
    fileprivate var dynamicData:[String:AnyObject]
    fileprivate let id:String
    
    enum InfoKey:String {
        case Identifier = "ConfigIdentifier"
    }
    
    convenience public init(fileURL:URL) {
        self.init(info:NSDictionary(contentsOf: fileURL) as! [String:AnyObject])
    }
    
    public init(info:[String:AnyObject]) {
        assert(info[InfoKey.Identifier.rawValue] != nil, "Config info has no identifier.")
        
        let userDefaults = UserDefaults.standard
        
        id = info[InfoKey.Identifier.rawValue]! as! String
        
        if let savedData = userDefaults.object(forKey: id) as? NSDictionary {
            dynamicData = savedData as! [String:AnyObject]
        } else {
            dynamicData = [String:AnyObject]()
        }
        
        rawData = info
    }
    
    // MARK: - Retrieving the values
    
    // Option 1
    open subscript(key:String) -> AnyObject? {
        set (value) {
            self.setValue(value, key:key)
        }
        get {
            return self.valueForKey(key)
        }
    }
    
    open subscript(key:CustomStringConvertible) -> AnyObject? {
        return self[key.description]
    }
    
    // Option 2
    open func valueForKey<R>(_ key:String) -> R? {
        // type must match or nil is returned.
        var outValue:R? = dynamicData[key] as? R
        
        if outValue == nil {
            outValue = rawData[key] as? R
        }
        
        return outValue
    }
    
    open func setValue<T:AnyObject>(_ value:T?, key:String) {
        assert(self.rawData[key] != nil, "Unknown key, config was not purposed to introduce new settings.")
        assert(key != InfoKey.Identifier.rawValue, "Config Identifier cannot change.")
        
        dynamicData[key] = value
    }
    
    open func reset() {
        dynamicData.removeAll(keepingCapacity: false)
    }
    
    open func restoreToDefaults() {
        let userDefaults = UserDefaults.standard
        
        reset()
        userDefaults.removeObject(forKey: id)
    }
    
    open func save() {
        let userDefaults = UserDefaults.standard
        let theData      = dynamicData as NSDictionary
        
        userDefaults.set(theData, forKey: id)
    }
}

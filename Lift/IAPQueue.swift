//
//  IAPManager.swift
//  Lift
//
//  Created by Sangwoo Im on 8/27/15.
//  Copyright © 2015 Sangwoo Im. All rights reserved.
//

import Foundation
import StoreKit

protocol IAPQueueDelegate {
    func IAPQueueRestoreDidFinish(_ success:Bool)
    func IAPQueueDidUpdateTransaction(_ transaction:IAPTransaction)
    func IAPQueueDidFail(_ transaction:IAPTransaction, error:IAPError)
}

open class IAPQueue : NSObject, SKPaymentTransactionObserver {
    fileprivate struct constants {
        static let shared = IAPQueue()
    }
    
    override init() {
        super.init()
        
        assert(false, "Use sharedInstance() instead.")
    }
    
    open func beginProcessingPendingTransactions() {
        let queue = SKPaymentQueue.default()
        queue.add(self)
    }
    
    open func endProcessingPendingTransactions() {
        let queue = SKPaymentQueue.default()
        queue.remove(self)
    }
    
    open class func sharedInstance() -> IAPQueue {
        return constants.shared
    }
    
    open var restored = Set<IAPTransaction>()
    open var pending  = [IAPTransaction]()
    open var current  : IAPTransaction?
    open var delegate : AnyObject?
    
    open func handleError(_ transaction:IAPTransaction, error:IAPError) {
        guard let delegateObj = delegate as? IAPQueueDelegate else {
            return
        }
        
        if current == transaction {
            current = pending.first
            pending.removeFirst()
        } else if let index = pending.index(of: transaction) {
            pending.remove(at: index)
        }
        
        delegateObj.IAPQueueDidFail(transaction, error:error)
    }
    
    open func start(_ product:SKProduct) {
        let aTransaction = IAPTransaction(product: product, queue: self)
        
        if current == nil {
            current = aTransaction
            
            do {
                try current?.start()
            } catch let failure as IAPError {
                handleError(aTransaction, error:failure)
            } catch let error as NSError {
                Logger.logWarning("\(error)")
            }
        } else {
            pending.append(aTransaction)
        }
    }
    
    open func finish(_ aTransaction:IAPTransaction) {
        current = pending.first
        
        pending.removeFirst()
        
        do {
            try aTransaction.finish()
        } catch let failure as IAPError {
            handleError(aTransaction, error:failure)
        } catch let error as NSError {
            Logger.logWarning("\(error)")
        }
    }
}

extension IAPQueue {
    public func paymentQueue(_ queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        
    }
    public func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        guard let iapDelegate = delegate as? IAPQueueDelegate else {
            return
        }
        
        iapDelegate.IAPQueueRestoreDidFinish(false)
        
        restored.removeAll()
    }
    public func paymentQueue(_ queue: SKPaymentQueue, updatedDownloads downloads: [SKDownload]) {
        
    }
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            if current == nil {
                current = pending.first
                
                if current != nil {
                    pending.removeFirst()
                }
            }
            
            guard let iapTransaction = current else {
                continue
            }
            
            if iapTransaction.paymentTransaction != transaction {
                continue
            }
            
            switch (transaction.transactionState) {
            case SKPaymentTransactionState.restored:
                restored.insert(iapTransaction)
                fallthrough
            case SKPaymentTransactionState.purchased:
                do {
                    try iapTransaction.fulfill()
                } catch let error as IAPError {
                    handleError(iapTransaction, error:error)
                } catch let error as NSError {
                    Logger.logWarning("\(error)")
                }
            case SKPaymentTransactionState.purchasing:
                break
            case SKPaymentTransactionState.failed:
                self.finish(iapTransaction)
            case SKPaymentTransactionState.deferred:
                break
            }
        }
    }
    public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        guard let iapDelegate = delegate as? IAPQueueDelegate else {
            return
        }
        var failed = 0
        
        for transaction in restored {
            if transaction.state != .delivered {
                failed += 1
            }
        }
        
        iapDelegate.IAPQueueRestoreDidFinish(failed == 0)
        restored.removeAll()
    }
}

//
//  Manager.swift
//  Mind Trails
//
//  Created by Sangwoo Im on 7/26/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

import Foundation

open class Manager {
    typealias NotificationHandler = (_ note:Notification?) -> ()
    
    // MARK: - Operation Queue
    public enum Mode : Int {
        case background, main
    }
    
    let mode:Mode
    
    var isRunning : Bool {
        get {
            return self.queue === OperationQueue.current
        }
    }
    
    class var typeName : String {
        return "Abstract Manager"
    }
    
    // Either main queue or serial queue depending on Mode set up at the time of the first call on the property.
    lazy var queue:OperationQueue = {
        var queue:OperationQueue? = nil
        
        switch self.mode {
        case .main :
            queue = OperationQueue.main
        case .background:
            fallthrough
        default:
            let bundle   = Bundle.main
            let bundleID = bundle.bundleIdentifier
            
            queue = OperationQueue()
            
            // init settings for the queue
            queue!.name                        = "\(bundleID).\(object_getClassName(self))"
            queue!.maxConcurrentOperationCount = 1
            queue!.isSuspended                   = false
        }
        
        return queue!
        }()
    
    // MARK: - App Notification
    public typealias StartCompletion = (_ result:AnyObject?, _ error:NSError?) -> ()
    
    // Application lifecycle event will be notified for each manager instance.
    // Manager register handlers if available at start time.
    // Conversely, unregisters at finish time.
    
    public enum AppEventType {
        case willResign
        case didBecomeActive
        case willEnterForeground
        case didEnterBackground
        case onMemoryWarning
        
        #if os(iOS)
        func notificationName() -> String {
            var outString:String?
            
            switch self {
            case .WillResign:
            outString = UIApplicationWillResignActiveNotification
            case .DidBecomeActive:
            outString = UIApplicationDidBecomeActiveNotification
            case .WillEnterForeground:
            outString = UIApplicationWillEnterForegroundNotification
            case .DidEnterBackground:
            outString = UIApplicationDidEnterBackgroundNotification
            case .OnMemoryWarning:
            outString = UIApplicationDidReceiveMemoryWarningNotification
            }
            
            return outString!
        }
        
        #elseif os(OSX)
        
        func notificationName() -> String {
            return ""
        }
        
        #endif
    }
    
    // This variable must be configured before start method is invoked.
    var notificationHandlers = [AppEventType:NotificationHandler]()
    // Tokens will be set up after start method is invoked.
    var handlerTokens        = [AppEventType:NSObjectProtocol]()
    // constant dictionary that was used for this manager when initialized.
    
    let managerInfo:Config?
    
    convenience public init(info:Config? = nil) {
        self.init(info:info, mode:.main)
    }
    
    public init(info:Config? = nil, mode:Mode) {
        managerInfo = info
        self.mode = mode
    }
    
    open func start(_ info:NSDictionary? = nil, completion: StartCompletion? = nil) {
        #if os(iOS)
            // start available app notification handlers
            let notifCenter = NSNotificationCenter.defaultCenter()
            // subclass should've have chosen the operation queue at this time.
            
            for (eventType, notHandler) in notificationHandlers {
            let name  = eventType.notificationName()
            let token = notifCenter.addObserverForName(name, object: nil, queue: self.queue, usingBlock: notHandler)
            
            handlerTokens[eventType] = token
            }
        #endif
    }
    
    open func finish() {
        #if os(iOS)
            // clean up things
            let notifCenter = NSNotificationCenter.defaultCenter()
            
            for (eventType, token) in handlerTokens {
            notifCenter.removeObserver(token, name: eventType.notificationName(), object:nil)
            }
        #endif
    }
    
    open func performSync(_ closure: @escaping () -> ()) {
        if !isRunning {
            performAsync(closure)
            queue.waitUntilAllOperationsAreFinished()
        } else {
            closure()
        }
    }
    
    open func performAsync(_ closure: @escaping () -> ()) {
        queue.addOperation(closure)
    }
    
    open func synchronize() {
        queue.waitUntilAllOperationsAreFinished()
    }
}
